#if !defined(PACKED_PROPERTIES_INCLUDED)
#define PACKED_PROPERTIES_INCLUDED

// modes: _PACKING_RGBA _PACKING_RGBS _PACKING_RGBA_MOS _PACKING_RGBS_MOXA

#if   defined(_PACKING_RGBA)
#elif defined(_PACKING_RGBS)
#elif defined(_PACKING_RGBA_MOS)
#elif defined(_PACKING_RGBS_MOXA)
#else
#define _PACKING_RGBA
#endif

#if defined(_PACKING_RGBS) || defined(_PACKING_RGBA_MOS) || defined(_PACKING_RGBS_MOXA)
#define SMOOTHNESS_FROM_TEX
#endif
#if defined(_PACKING_RGBA_MOS) || defined(_PACKING_RGBS_MOXA)
#define PACKED_TEX
#endif

// properties

fixed4 _Color;

#ifdef SMOOTHNESS_FROM_TEX
half _GlossMapScale;
#else
half _Glossiness;
#endif

#ifdef PACKED_TEX
half _OcclusionStrength;
#else
half _Metallic;
#endif

#ifdef _NORMALMAP_SCALE
half _BumpScale;
#endif

// getters

#define GET_PACKED_ALBEDO         color.rgb * _Color.rgb
#if defined(_PACKING_RGBS)
    #define GET_PACKED_ALPHA      _Color.a
    #define GET_PACKED_METALLIC   _Metallic
    #define GET_PACKED_OCCLUSION  1.0
    #define GET_PACKED_SMOOTHNESS color.a * _GlossMapScale;
#elif defined(_PACKING_RGBA_MOS)
    #define GET_PACKED_ALPHA      color.a * _Color.a
    #define GET_PACKED_METALLIC   packed.r
    #define GET_PACKED_OCCLUSION  LerpOneTo(packed.g, _OcclusionStrength)
    #define GET_PACKED_SMOOTHNESS packed.b * _GlossMapScale;
#elif defined(_PACKING_RGBS_MOXA)
    #define GET_PACKED_ALPHA      packed.a * _Color.a
    #define GET_PACKED_METALLIC   packed.r
    #define GET_PACKED_OCCLUSION  LerpOneTo(packed.g, _OcclusionStrength)
    #define GET_PACKED_SMOOTHNESS packed.b * _GlossMapScale;
#else // _PACKING_RGBA
    #define GET_PACKED_ALPHA      _Color.a
    #define GET_PACKED_METALLIC   _Metallic
    #define GET_PACKED_OCCLUSION  1.0
    #define GET_PACKED_SMOOTHNESS _Glossiness
#endif

#define TRANSFER_PACKED(o) \
	o.Albedo     = GET_PACKED_ALBEDO; \
	o.Alpha      = GET_PACKED_ALPHA; \
	o.Metallic   = GET_PACKED_METALLIC; \
	o.Smoothness = GET_PACKED_SMOOTHNESS; \
	o.Occlusion  = GET_PACKED_OCCLUSION;

#endif // PACKED_PROPERTIES_INCLUDED
