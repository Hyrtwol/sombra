#if !defined(VERTEXTILESDATA_INCLUDED)
#define VERTEXTILESDATA_INCLUDED

// UNITY_PASS_FORWARDBASE   Forward rendering base pass (main directional light, lightmaps, SH).
// UNITY_PASS_FORWARDADD    Forward rendering additive pass (one light per pass).
// UNITY_PASS_DEFERRED      Deferred shadingpass (renders g buffer).
// UNITY_PASS_SHADOWCASTER  Shadow caster and depth Texture rendering pass.
// UNITY_PASS_PREPASSBASE   Legacy deferred lighting base pass (renders normals and specular exponent).
// UNITY_PASS_PREPASSFINAL  Legacy deferred lighting final pass (applies lighting and Textures).

// vertex-to-geometry interpolation data
struct v2g_surf_trio
{
#if defined(UNITY_PASS_SHADOWCASTER)
    NOT USED V2F_SHADOW_CASTER;
#else

    V2G_DATA

#if defined(UNITY_PASS_FORWARDBASE)

  #ifndef LIGHTMAP_ON
    #if UNITY_SHOULD_SAMPLE_SH
      CAULDRON_DECLARE_SH(6)
      #define HAVE_SH
    #endif
  #else
    CAULDRON_DECLARE_LMAP(6)
    #define HAVE_LMAP
  #endif

  #ifdef UNITY_HALF_PRECISION_FRAGMENT_SHADER_REGISTERS
    UNITY_LIGHTING_COORDS(7,8)
    #define HAVE_LIGHT
    #define HAVE_SHADOW
    //#if !defined (DIRECTIONAL)
    //#endif
  #else
    UNITY_FOG_COORDS(7)
    #if defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2)
    #define HAVE_FOG
    #endif
    UNITY_SHADOW_COORDS(8)
    #define HAVE_SHADOW
    //#if !defined (DIRECTIONAL)
    //#endif
  #endif

  #ifndef LIGHTMAP_ON
    #if SHADER_TARGET >= 30
      CAULDRON_DECLARE_LMAP(9)
      #define HAVE_LMAP
    #endif
  #else
    #ifdef DIRLIGHTMAP_COMBINED
      CAULDRON_DECLARE_TSPACE3(9, 10, 11)
      #define HAVE_TSPACE
    #endif
  #endif

#elif defined(UNITY_PASS_FORWARDADD)

  UNITY_LIGHTING_COORDS(6,7)
  #if !defined (DIRECTIONAL)
  #define HAVE_LIGHT
  #endif
  #define HAVE_SHADOW
  UNITY_FOG_COORDS(8)
  #if defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2)
  #define HAVE_FOG
  #endif

#elif defined(UNITY_PASS_DEFERRED)

  #ifndef DIRLIGHTMAP_OFF
    CAULDRON_DECLARE_VIEWDIR(6)
    #define HAVE_VIEWDIR
  #endif
  CAULDRON_DECLARE_LMAP(7)
  #define HAVE_LMAP
  #ifndef LIGHTMAP_ON
    #if UNITY_SHOULD_SAMPLE_SH && !UNITY_SAMPLE_FULL_SH_PER_PIXEL
      CAULDRON_DECLARE_SH(8)
      #define HAVE_SH
    #endif
  #else
    #ifdef DIRLIGHTMAP_OFF
      CAULDRON_DECLARE_LMAP_FADEPOS(8)
      #define HAVE_LMAP_FADEPOS
    #endif
  #endif

#elif defined(UNITY_PASS_META)

  V2F_DECLARE_EDITOR_VISUALIZATION(5,6)

#endif

  UNITY_VERTEX_INPUT_INSTANCE_ID
  UNITY_VERTEX_OUTPUT_STEREO

#endif // UNITY_PASS_SHADOWCASTER
};

// geometry-to-fragment interpolation data
struct g2f_surf_trio
{
#if defined(UNITY_PASS_SHADOWCASTER)
    NOT USED V2F_SHADOW_CASTER;
#else

    G2F_DATA

#if defined(UNITY_PASS_FORWARDBASE)

  #ifndef LIGHTMAP_ON
    #if UNITY_SHOULD_SAMPLE_SH
      CAULDRON_DECLARE_SH(6)
      #define HAVE_SH
    #endif
  #else
    CAULDRON_DECLARE_LMAP(6)
    #define HAVE_LMAP
  #endif

  #ifdef UNITY_HALF_PRECISION_FRAGMENT_SHADER_REGISTERS
    UNITY_LIGHTING_COORDS(7,8)
    #define HAVE_LIGHT
    #define HAVE_SHADOW
    //#if !defined (DIRECTIONAL)
    //#endif
  #else
    UNITY_FOG_COORDS(7)
    #if defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2)
    #define HAVE_FOG
    #endif
    UNITY_SHADOW_COORDS(8)
    #define HAVE_SHADOW
    //#if !defined (DIRECTIONAL)
    //#endif
  #endif

  #ifndef LIGHTMAP_ON
    #if SHADER_TARGET >= 30
      CAULDRON_DECLARE_LMAP(9)
      #define HAVE_LMAP
    #endif
  #else
    #ifdef DIRLIGHTMAP_COMBINED
      CAULDRON_DECLARE_TSPACE3(9,10,11)
      #define HAVE_TSPACE
    #endif
  #endif

#elif defined(UNITY_PASS_FORWARDADD)

  UNITY_LIGHTING_COORDS(6,7)
  #if !defined (DIRECTIONAL)
  #define HAVE_LIGHT
  #endif
  #define HAVE_SHADOW
  UNITY_FOG_COORDS(8)
  #if defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2)
  #define HAVE_FOG
  #endif

#elif defined(UNITY_PASS_DEFERRED)

  #ifndef DIRLIGHTMAP_OFF
    CAULDRON_DECLARE_VIEWDIR(6)
    #define HAVE_VIEWDIR
  #endif
  CAULDRON_DECLARE_LMAP(7)
  #define HAVE_LMAP
  #ifndef LIGHTMAP_ON
    #if UNITY_SHOULD_SAMPLE_SH && !UNITY_SAMPLE_FULL_SH_PER_PIXEL
      CAULDRON_DECLARE_SH(8)
      #define HAVE_SH
    #endif
  #else
    #ifdef DIRLIGHTMAP_OFF
      CAULDRON_DECLARE_LMAP_FADEPOS(8)
      #define HAVE_LMAP_FADEPOS
    #endif
  #endif

#elif defined(UNITY_PASS_META)

  V2F_DECLARE_EDITOR_VISUALIZATION(5,6)

#endif

  UNITY_VERTEX_INPUT_INSTANCE_ID
  UNITY_VERTEX_OUTPUT_STEREO

#endif // UNITY_PASS_SHADOWCASTER
};

#endif // VERTEXTILESDATA_INCLUDED
