Shader "Cauldron/VertexTiles/VertexTilesPackedBake" {
Properties {
    _Color("Color", Color) = (1,1,1,1)
    _MainTex("Albedo (RGB 2DArray)", 2DArray) = "white" {}

    [NoScaleOffset] _PackedTex("Packed (RGBA 2DArray)", 2DArray) = "black" {}
    [KeywordEnum(RGBA, RGBS, RGBA_MOS, RGBS_MOXA)] _Packing("Packing", Int) = 0

    _Glossiness("Smoothness", Range(0,1)) = 0.5
    _GlossMapScale("Smoothness Scale", Range(0.0, 1.0)) = 1.0
    _Metallic("Metallic", Range(0,1)) = 0.0
    _OcclusionStrength("Occlusion", Range(0.0, 1.0)) = 1.0

    _TexScale("Texture Scale", Float) = 1.0
    [PowerSlider(3.0)] _TexBlend("Blend Sharpness", Range(1,64)) = 2.0

    [IntRange] _TexIdx("Texture Index", Range(0, 15)) = 0
}
SubShader{
    Tags { "RenderType" = "Opaque" }
    LOD 200
CGPROGRAM
#pragma surface surf_trio Standard fullforwardshadows addshadow vertex:vert_trio
#pragma require geometry
#pragma require 2darray
#pragma target 4.0
#pragma shader_feature_local _ _PACKING_RGBS _PACKING_RGBA_MOS _PACKING_RGBS_MOXA
#include "../../_Sombra/Runtime/Shaders/VertexTiles/VertexTiles.cginc"

int _TexIdx;

struct Input
{
    float4 tparam;
    float3 tpos;
    float3 tnormal;
    float3 ttangent;
};

void vert_trio(inout appdata v, out Input o)
{
    VT_TRANSFER_APPDATA(v, o)
}

void surf_trio(Input IN, inout SurfaceOutputStandard o)
{
    float3 uvi = float3(IN.tparam.xy, _TexIdx);
    fixed4 color = UNITY_SAMPLE_TEX2DARRAY(_MainTex, uvi) * _Color;
#ifdef PACKED_TEX
    fixed4 packed = UNITY_SAMPLE_TEX2DARRAY(_PackedTex, uvi);
#endif
    TRANSFER_PACKED(o)
#ifdef _NORMALMAP
    //o.Normal = float3(0, 0, 1);
    //o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
    o.Normal = UnpackNormal(UNITY_SAMPLE_TEX2DARRAY(_NormalMap, uvi));
    //o.Normal = UnpackScaleNormal(UNITY_SAMPLE_TEX2DARRAY(_NormalMap, uvi), _MOHS_Strength.w);
#endif
}

ENDCG
}
FallBack "Diffuse"
}
