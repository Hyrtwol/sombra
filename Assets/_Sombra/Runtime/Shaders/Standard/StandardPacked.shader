Shader "Cauldron/Standard/StandardPacked"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGBA)", 2D) = "white" {}

        [NoScaleOffset] _PackedTex("Packed (RGBA)", 2D) = "black" {}
        [KeywordEnum(RGBA, RGBS, RGBA_MOS, RGBS_MOXA)] _Packing("Packing", Int) = 0

        [Gamma] _Metallic("Metallic", Range(0.0, 1.0)) = 0.0
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _GlossMapScale("Smoothness Scale", Range(0.0, 1.0)) = 1.0
        _OcclusionStrength("Occlusion", Range(0.0, 1.0)) = 1.0

        [Toggle(_NORMALMAP)] _UseNormalMap("Use Normal Map", Float) = 0
        [NoScaleOffset][Normal] _BumpMap("Normal Map", 2D) = "bump" {}
        _BumpScale("Normal Map Scale", Float) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200
        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows
        #pragma target 3.0
        #pragma shader_feature_local _ _PACKING_RGBS _PACKING_RGBA_MOS _PACKING_RGBS_MOXA
        //#pragma shader_feature_local_fragment _NORMALMAP
        #pragma shader_feature_local _NORMALMAP
        //#define _NORMALMAP
#ifdef _NORMALMAP
        #define _NORMALMAP_SCALE
#endif
        #include "StandardPacked.cginc"
        ENDCG
    }
    FallBack "Diffuse"
    CustomEditor "Sombra.SombraShaderGUI"
    //CustomEditor "Sombra.StandardPackedShaderGUI"
}
