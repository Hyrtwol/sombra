# Sombra a la Cour

![Sombra](Content/Images/SombraBanner.jpg)

A collection of Unity shaders.

Package git url:
```
"dk.hyrtwol.sombra": "ssh://git@bitbucket.org/Hyrtwol/sombra.git?path=/Assets/_Sombra#v1.0.1",
"dk.hyrtwol.sombra": "ssh://git@bitbucket.org/Hyrtwol/sombra.git?path=/Assets/_Sombra#master"
```

## Textures

### UV Checkers

name               | texture
------------------ | -------
uv_checker_x       | ![](Assets/_Sombra/Textures/uv_checker_x.png)
uv_checker_y       | ![](Assets/_Sombra/Textures/uv_checker_y.png)
uv_checker_z       | ![](Assets/_Sombra/Textures/uv_checker_z.png)
uv_checker_w       | ![](Assets/_Sombra/Textures/uv_checker_w.png)
uv_checker_n       | ![](Assets/_Sombra/Textures/uv_checker_n.png)
uv_checker_o       | ![](Assets/_Sombra/Textures/uv_checker_o.png)
uv_checker_h       | ![](Assets/_Sombra/Textures/uv_checker_h.png)
uv_checker_cubic   | ![](Assets/_Sombra/Textures/uv_checker_cubic.png)
uv_checker_3d      | ![](Assets/_Sombra/Textures/uv_checker_3d.png)
uv_checker_array   | ![](Assets/_Sombra/Textures/uv_checker_array.png)
uv_checker_array_n | ![](Assets/_Sombra/Textures/uv_checker_array_n.png)
uv_checker_1m      | ![](Assets/_Sombra/Textures/uv_checker_1m.png)

## Deploy

```
git subtree push --prefix Assets/_Sombra origin package
```

------------------
_-= end of doc =-_
