Shader "Cauldron/Projection/Planar"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        [Enum(X,0,Y,1,Z,2)] _U("TexCoord U element", Int) = 0
        [Enum(X,0,Y,1,Z,2)] _V("TexCoord V element", Int) = 1
        [KeywordEnum(ObjectSpace, WorldSpace)] _UVSource("UV Source", Int) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert

        #pragma multi_compile _ _UVSOURCE_WORLDSPACE
        #pragma target 3.0 // Use shader model 3.0 target, to get nicer looking lighting

        sampler2D _MainTex;

        struct Input
        {
            float2 lookup;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        int _U, _V;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void vert(inout appdata_full v, out Input o)
        {
#if _UVSOURCE_WORLDSPACE
            float3 pos = mul(unity_ObjectToWorld, v.vertex).xyz;
#else
            float3 pos = v.vertex.xyz;
#endif
            o.lookup = float2(pos[_U], pos[_V]);
        }

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 c = tex2D (_MainTex, IN.lookup) * _Color;
            o.Albedo = c.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
    CustomEditor "Sombra.SombraShaderGUI"
}
