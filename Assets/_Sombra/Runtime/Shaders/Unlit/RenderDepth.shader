// https://docs.unity3d.com/Manual/SL-DepthTextures.html
// https://chrismflynn.wordpress.com/2012/09/06/fun-with-shaders-and-the-depth-buffer/
Shader "Cauldron/Unlit/RenderDepth"
{
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {

            CGPROGRAM
            #pragma target 3.0
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            uniform sampler2D _CameraDepthTexture; //the depth texture

            struct v2f
            {
                float4 pos : SV_POSITION;
                float4 projPos : TEXCOORD0; //Screen position of pos
            };

            v2f vert(appdata_base v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.projPos = ComputeScreenPos(o.pos);
                return o;
            }

			fixed4 frag(v2f i) : COLOR
            {
                //Grab the depth value from the depth texture
                //Linear01Depth restricts this value to [0, 1]
                float depth = Linear01Depth(
					tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)).r
				);
                return fixed4(depth,depth,depth,1.0);
            }

            ENDCG
        }
    }
    FallBack "VertexLit"
}
