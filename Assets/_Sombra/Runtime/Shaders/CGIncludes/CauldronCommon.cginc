#if !defined(CAULDRON_COMMON_INCLUDED)
#define CAULDRON_COMMON_INCLUDED

#define CAULDRON_DECLARE_VIEWDIR(idx) float3 viewDir : TEXCOORD##idx;
#define CAULDRON_DECLARE_EDITOR_VISUALIZATION(idx, idx2) float2 vizUV : TEXCOORD##idx; float4 lightCoord : TEXCOORD##idx2;

#define CAULDRON_DECLARE_SH(idx) half3 sh : TEXCOORD##idx;
#define CAULDRON_DECLARE_LMAP_FADEPOS(idx) float4 lmapFadePos : TEXCOORD##idx;
#define CAULDRON_DECLARE_LMAP(idx) float4 lmap : TEXCOORD##idx;

#if UNITY_SHOULD_SAMPLE_SH
#define V2F_DECLARE_SH(idx) CAULDRON_DECLARE_SH(idx)
#else
#define V2F_DECLARE_SH(idx)
#endif

#define CAULDRON_DECLARE_WORLD_POS(idx) float3 worldPos : TEXCOORD##idx;
#define CAULDRON_DECLARE_WORLD_NORMAL(idx) float3 worldNormal : TEXCOORD##idx;
#define CAULDRON_DECLARE_WORLD_POS_NORMAL(i0, i1) \
    CAULDRON_DECLARE_WORLD_NORMAL(i0) \
    CAULDRON_DECLARE_WORLD_POS(i1)

#define CAULDRON_DECLARE_TSPACE3(i0, i1, i2) \
    float3 tSpace0  : TEXCOORD##i0; \
    float3 tSpace1  : TEXCOORD##i1; \
    float3 tSpace2  : TEXCOORD##i2;

#define CAULDRON_DECLARE_TSPACE4(i0, i1, i2) \
    float4 tSpace0  : TEXCOORD##i0; \
    float4 tSpace1  : TEXCOORD##i1; \
    float4 tSpace2  : TEXCOORD##i2;

#define CAULDRON_DECLARE_TSPACE3_WPOS(i0, i1, i2, i3) \
    CAULDRON_DECLARE_TSPACE3(i0, i1, i2) \
    CAULDRON_DECLARE_WORLD_POS(i3)

#if DIRLIGHTMAP_OFF
#define V2F_DECLARE_LMAP_FADEPOS(idx) CAULDRON_DECLARE_LMAP_FADEPOS(idx);
#define V2F_DECLARE_VIEWDIR(idx)
#else
#define V2F_DECLARE_LMAP_FADEPOS(idx)
#define V2F_DECLARE_VIEWDIR(idx) CAULDRON_DECLARE_VIEWDIR(idx);
#endif

#if EDITOR_VISUALIZATION
#define V2F_DECLARE_EDITOR_VISUALIZATION(idx, idx2) CAULDRON_DECLARE_EDITOR_VISUALIZATION(idx, idx2);
#else
#define V2F_DECLARE_EDITOR_VISUALIZATION(idx, idx2)
#endif



//#if defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2)
////#define UNITY_FOG_COORDS(idx) UNITY_FOG_COORDS_PACKED(idx, float1)
////#define G2F_TRANSFER_FOG(o, i) o.fogCoord = i.fogCoord;
//#define HAVE_FOG
//
//#if (SHADER_TARGET < 30) || defined(SHADER_API_MOBILE)
//    // mobile or SM2.0: calculate fog factor per-vertex
////#define UNITY_TRANSFER_FOG(o,outpos) UNITY_CALC_FOG_FACTOR((outpos).z); o.fogCoord.x = unityFogFactor
////#define UNITY_TRANSFER_FOG_COMBINED_WITH_TSPACE(o,outpos) UNITY_CALC_FOG_FACTOR((outpos).z); o.tSpace1.y = tangentSign; o.tSpace2.y = unityFogFactor
////#define UNITY_TRANSFER_FOG_COMBINED_WITH_WORLD_POS(o,outpos) UNITY_CALC_FOG_FACTOR((outpos).z); o.worldPos.w = unityFogFactor
////#define UNITY_TRANSFER_FOG_COMBINED_WITH_EYE_VEC(o,outpos) UNITY_CALC_FOG_FACTOR((outpos).z); o.eyeVec.w = unityFogFactor
//#else
//    // SM3.0 and PC/console: calculate fog distance per-vertex, and fog factor per-pixel
////#define UNITY_TRANSFER_FOG(o,outpos) o.fogCoord.x = (outpos).z
////#define UNITY_TRANSFER_FOG_COMBINED_WITH_TSPACE(o,outpos) o.tSpace2.y = (outpos).z
////#define UNITY_TRANSFER_FOG_COMBINED_WITH_WORLD_POS(o,outpos) o.worldPos.w = (outpos).z
////#define UNITY_TRANSFER_FOG_COMBINED_WITH_EYE_VEC(o,outpos) o.eyeVec.w = (outpos).z
//#endif
//
//#else
////#define G2F_TRANSFER_FOG(o, i)
//
////#define UNITY_FOG_COORDS(idx)
////#define UNITY_TRANSFER_FOG(o,outpos)
////#define UNITY_TRANSFER_FOG_COMBINED_WITH_TSPACE(o,outpos)
////#define UNITY_TRANSFER_FOG_COMBINED_WITH_WORLD_POS(o,outpos)
////#define UNITY_TRANSFER_FOG_COMBINED_WITH_EYE_VEC(o,outpos)
//#endif


#endif // CAULDRON_COMMON_INCLUDED
