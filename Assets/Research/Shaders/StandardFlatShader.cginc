#if !defined(STANDARDFLAT)
#define STANDARDFLAT

// UNITY_PASS_FORWARDBASE   Forward rendering base pass (main directional light, lightmaps, SH).
// UNITY_PASS_FORWARDADD    Forward rendering additive pass (one light per pass).
// UNITY_PASS_DEFERRED      Deferred shadingpass (renders g buffer).
// UNITY_PASS_SHADOWCASTER  Shadow caster and depth Texture rendering pass.
// UNITY_PASS_PREPASSBASE   Legacy deferred lighting base pass (renders normals and specular exponent).
// UNITY_PASS_PREPASSFINAL  Legacy deferred lighting final pass (applies lighting and Textures).

#include "HLSLSupport.cginc"
#define UNITY_INSTANCED_LOD_FADE
#define UNITY_INSTANCED_SH
#define UNITY_INSTANCED_LIGHTMAPSTS
#include "UnityShaderVariables.cginc"
#include "UnityShaderUtilities.cginc"

#include "UnityCG.cginc"
#include "Lighting.cginc"
#include "UnityPBSLighting.cginc"
#if defined(UNITY_PASS_FORWARDBASE) || defined(UNITY_PASS_FORWARDADD)
#include "AutoLight.cginc"
#endif
#if defined(UNITY_PASS_META)
#include "UnityMetaPass.cginc"
#endif

sampler2D _MainTex;
float4 _MainTex_ST;

half _Glossiness;
half _Metallic;
fixed4 _Color;

#if defined(UNITY_PASS_DEFERRED)
  #ifdef LIGHTMAP_ON
    float4 unity_LightmapFade;
  #endif
  fixed4 unity_Ambient;
#endif

struct Input
{
    float2 uv_MainTex;
    float3 normal;
};

#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))

UNITY_INSTANCING_BUFFER_START(Props)
UNITY_INSTANCING_BUFFER_END(Props)

#ifdef UNITY_HALF_PRECISION_FRAGMENT_SHADER_REGISTERS
#define FOG_COMBINED_WITH_TSPACE
#endif

void vert(inout appdata_full v, out Input o)
{
    o.uv_MainTex = v.texcoord;
    o.normal = v.normal;
}

void surf(Input IN, inout SurfaceOutputStandard o)
{
    // Albedo comes from a texture tinted by color
    fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
    //c.rgb *= IN.normal * 0.5 + 0.5;
    //c.rgb = IN.normal * 0.5 + 0.5;
    o.Albedo = c.rgb;
    // Metallic and smoothness come from slider variables
    o.Metallic = _Metallic;
    o.Smoothness = _Glossiness;
    o.Alpha = c.a;
    // tangent space normal, if written
    o.Normal = float3(0, 0, 1); // make sure that surface shader compiler realizes we write to normal
}

// vertex-to-fragment interpolation data

struct v2f_surf {
    UNITY_POSITION(pos);
    float2 pack0 : TEXCOORD0; // _MainTex
    float4 tSpace0 : TEXCOORD1;
    float4 tSpace1 : TEXCOORD2;
    float4 tSpace2 : TEXCOORD3;
    float3 custompack0 : TEXCOORD4; // normal
#if defined(UNITY_PASS_FORWARDBASE)

  #ifndef LIGHTMAP_ON
    #if UNITY_SHOULD_SAMPLE_SH
      half3 sh : TEXCOORD5; // SH
    #endif
  #else
      float4 lmap : TEXCOORD5;
  #endif
  #ifdef UNITY_HALF_PRECISION_FRAGMENT_SHADER_REGISTERS
      UNITY_LIGHTING_COORDS(6, 7)
  #else
      UNITY_FOG_COORDS(6)
      UNITY_SHADOW_COORDS(7)
  #endif
  #ifndef LIGHTMAP_ON
    #if SHADER_TARGET >= 30
      float4 lmap : TEXCOORD8;
    #endif
  #endif

#elif defined(UNITY_PASS_FORWARDADD)

  UNITY_LIGHTING_COORDS(6, 7)
  UNITY_FOG_COORDS(8)

#elif defined(UNITY_PASS_DEFERRED)

  #ifndef DIRLIGHTMAP_OFF
    float3 viewDir : TEXCOORD5;
  #endif
    float4 lmap : TEXCOORD6;
  #ifndef LIGHTMAP_ON
    #if UNITY_SHOULD_SAMPLE_SH && !UNITY_SAMPLE_FULL_SH_PER_PIXEL
      half3 sh : TEXCOORD7; // SH
    #endif
  #else
    #ifdef DIRLIGHTMAP_OFF
      float4 lmapFadePos : TEXCOORD7;
    #endif
  #endif

#elif defined(UNITY_PASS_META)

  #ifdef EDITOR_VISUALIZATION
    float2 vizUV : TEXCOORD5;
    float4 lightCoord : TEXCOORD6;
  #endif

#endif

    UNITY_VERTEX_INPUT_INSTANCE_ID
    UNITY_VERTEX_OUTPUT_STEREO
};

// vertex shader
v2f_surf vert_surf(appdata_full v) {
  UNITY_SETUP_INSTANCE_ID(v);
  v2f_surf o;
  UNITY_INITIALIZE_OUTPUT(v2f_surf, o);
  UNITY_TRANSFER_INSTANCE_ID(v, o);
  UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
  {
      Input customInputData;
      vert(v, customInputData);
      o.custompack0.xyz = customInputData.normal;
  }
  #if defined(UNITY_PASS_META)
    o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST);
  #else
    o.pos = UnityObjectToClipPos(v.vertex);
  #endif

  o.pack0.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
  float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
  float3 worldNormal = UnityObjectToWorldNormal(v.normal);
  fixed3 worldTangent = UnityObjectToWorldDir(v.tangent.xyz);
  fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
  fixed3 worldBinormal = cross(worldNormal, worldTangent) * tangentSign;

  o.tSpace0 = float4(worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x);
  o.tSpace1 = float4(worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y);
  o.tSpace2 = float4(worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z);

#if defined(UNITY_PASS_DEFERRED)
  #ifndef DIRLIGHTMAP_OFF
    float3 viewDirForLight = UnityWorldSpaceViewDir(worldPos);
    o.viewDir.x = dot(viewDirForLight, worldTangent);
    o.viewDir.y = dot(viewDirForLight, worldBinormal);
    o.viewDir.z = dot(viewDirForLight, worldNormal);
  #endif
#endif

#if defined(UNITY_PASS_FORWARDBASE) || defined(UNITY_PASS_DEFERRED)
  #ifdef DYNAMICLIGHTMAP_ON
    o.lmap.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
  #else
    o.lmap.zw = 0;
  #endif
  #ifdef LIGHTMAP_ON
    o.lmap.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
    #if defined(UNITY_PASS_DEFERRED)
      #ifdef DIRLIGHTMAP_OFF
        o.lmapFadePos.xyz = (mul(unity_ObjectToWorld, v.vertex).xyz - unity_ShadowFadeCenterAndType.xyz) * unity_ShadowFadeCenterAndType.w;
        o.lmapFadePos.w = (-UnityObjectToViewPos(v.vertex).z) * (1.0 - unity_ShadowFadeCenterAndType.w);
      #endif
    #endif
  #else
    #if defined(UNITY_PASS_DEFERRED)
    o.lmap.xy = 0;
    #endif

    #if UNITY_SHOULD_SAMPLE_SH && !UNITY_SAMPLE_FULL_SH_PER_PIXEL
      o.sh = 0;
      #if defined(UNITY_PASS_FORWARDBASE)
        // Approximated illumination from non-important point lights
        #ifdef VERTEXLIGHT_ON
          o.sh += Shade4PointLights(
                unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
                unity_LightColor[0].rgb, unity_LightColor[1].rgb, unity_LightColor[2].rgb, unity_LightColor[3].rgb,
                unity_4LightAtten0, worldPos, worldNormal);
        #endif
      #endif
      o.sh = ShadeSHPerVertex(worldNormal, o.sh);
    #endif
  #endif // LIGHTMAP_ON

#endif

#if defined(UNITY_PASS_FORWARDBASE) || defined(UNITY_PASS_FORWARDADD)
  UNITY_TRANSFER_LIGHTING(o, v.texcoord1.xy); // pass shadow and, possibly, light cookie coordinates to pixel shader

  #ifdef FOG_COMBINED_WITH_TSPACE
    UNITY_TRANSFER_FOG_COMBINED_WITH_TSPACE(o, o.pos); // pass fog coordinates to pixel shader
  #elif defined (FOG_COMBINED_WITH_WORLD_POS)
    UNITY_TRANSFER_FOG_COMBINED_WITH_WORLD_POS(o, o.pos); // pass fog coordinates to pixel shader
  #else
    UNITY_TRANSFER_FOG(o, o.pos); // pass fog coordinates to pixel shader
  #endif
#endif

#if defined(UNITY_PASS_META)
  #ifdef EDITOR_VISUALIZATION
    o.vizUV = 0;
    o.lightCoord = 0;
    if (unity_VisualizationMode == EDITORVIZ_TEXTURE)
        o.vizUV = UnityMetaVizUV(unity_EditorViz_UVIndex, v.texcoord.xy, v.texcoord1.xy, v.texcoord2.xy, unity_EditorViz_Texture_ST);
    else if (unity_VisualizationMode == EDITORVIZ_SHOWLIGHTMASK)
    {
        o.vizUV = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
        o.lightCoord = mul(unity_EditorViz_WorldToLight, mul(unity_ObjectToWorld, float4(v.vertex.xyz, 1)));
    }
  #endif
#endif

    return o;
}

#define GET_TANGENT(IN) float3(IN.tSpace0.x, IN.tSpace1.x, IN.tSpace2.x)
#define GET_NORMAL(IN) float3(IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z)
#define GET_WORLDPOS(IN) float3(IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w)

// geometry shader
[maxvertexcount(3)]
void geom_surf(triangle v2f_surf input[3], inout TriangleStream<v2f_surf> triStream)
{
    //     1      0-1-2
    //    / \     1-2-0
    //   0---2    2-0-1

    float3 worldPos0 = GET_WORLDPOS(input[0]);
    float3 worldPos1 = GET_WORLDPOS(input[1]);
    float3 worldPos2 = GET_WORLDPOS(input[2]);
    float3 trinormal = cross(normalize(worldPos1-worldPos0), normalize(worldPos2-worldPos0));
    input[0].custompack0 = trinormal;
    input[1].custompack0 = trinormal;
    input[2].custompack0 = trinormal;

    float3 worldNormal = trinormal;
    {
        float3 worldTangent = GET_TANGENT(input[0]);
        float3 worldBinormal = cross(worldNormal, worldTangent);
        input[0].tSpace0 = float4(worldTangent.x, worldBinormal.x, worldNormal.x, worldPos0.x);
        input[0].tSpace1 = float4(worldTangent.y, worldBinormal.y, worldNormal.y, worldPos0.y);
        input[0].tSpace2 = float4(worldTangent.z, worldBinormal.z, worldNormal.z, worldPos0.z);
    }
    {
        float3 worldTangent = GET_TANGENT(input[1]);
        float3 worldBinormal = cross(worldNormal, worldTangent);
        input[1].tSpace0 = float4(worldTangent.x, worldBinormal.x, worldNormal.x, worldPos1.x);
        input[1].tSpace1 = float4(worldTangent.y, worldBinormal.y, worldNormal.y, worldPos1.y);
        input[1].tSpace2 = float4(worldTangent.z, worldBinormal.z, worldNormal.z, worldPos1.z);
    }
    {
        float3 worldTangent = GET_TANGENT(input[2]);
        float3 worldBinormal = cross(worldNormal, worldTangent);
        input[2].tSpace0 = float4(worldTangent.x, worldBinormal.x, worldNormal.x, worldPos2.x);
        input[2].tSpace1 = float4(worldTangent.y, worldBinormal.y, worldNormal.y, worldPos2.y);
        input[2].tSpace2 = float4(worldTangent.z, worldBinormal.z, worldNormal.z, worldPos2.z);
    }

    triStream.Append(input[0]);
    triStream.Append(input[1]);
    triStream.Append(input[2]);

    //float3 worldNormal = UnityObjectToWorldNormal(v.normal);
}

// fragment shader
#if defined(UNITY_PASS_DEFERRED)
void frag_surf (v2f_surf IN,
    out half4 outGBuffer0 : SV_Target0,
    out half4 outGBuffer1 : SV_Target1,
    out half4 outGBuffer2 : SV_Target2,
    out half4 outEmission : SV_Target3
#if defined(SHADOWS_SHADOWMASK) && (UNITY_ALLOWED_MRT_COUNT > 4)
    , out half4 outShadowMask : SV_Target4
#endif
)
#else
fixed4 frag_surf (v2f_surf IN) : SV_Target
#endif
{
  UNITY_SETUP_INSTANCE_ID(IN);
  // prepare and unpack data
  Input surfIN;
  #ifdef FOG_COMBINED_WITH_TSPACE
    UNITY_EXTRACT_FOG_FROM_TSPACE(IN);
  #elif defined (FOG_COMBINED_WITH_WORLD_POS)
    UNITY_EXTRACT_FOG_FROM_WORLD_POS(IN);
  #else
    UNITY_EXTRACT_FOG(IN);
  #endif
  #ifdef FOG_COMBINED_WITH_TSPACE
    UNITY_RECONSTRUCT_TBN(IN);
  #else
    UNITY_EXTRACT_TBN(IN);
  #endif
  UNITY_INITIALIZE_OUTPUT(Input,surfIN);
  {
      surfIN.uv_MainTex = IN.pack0.xy;
      surfIN.normal = IN.custompack0.xyz;
  }
  float3 worldPos = float3(IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w);
  #ifndef USING_DIRECTIONAL_LIGHT
    fixed3 lightDir = normalize(UnityWorldSpaceLightDir(worldPos));
  #else
    fixed3 lightDir = _WorldSpaceLightPos0.xyz;
  #endif
  float3 worldViewDir = normalize(UnityWorldSpaceViewDir(worldPos));

  SurfaceOutputStandard o;
  UNITY_INITIALIZE_OUTPUT(SurfaceOutputStandard, o);
  o.Albedo = 0.0;
  o.Emission = 0.0;
  o.Alpha = 0.0;
  o.Occlusion = 1.0;
  //fixed3 normalWorldVertex = fixed3(0,0,1);
  o.Normal = fixed3(0,0,1);

  // call surface function
  surf (surfIN, o);

  // compute lighting & shadowing factor
#if defined(UNITY_PASS_META)
  UnityMetaInput metaIN;
  UNITY_INITIALIZE_OUTPUT(UnityMetaInput, metaIN);
  metaIN.Albedo = o.Albedo;
  metaIN.Emission = o.Emission;
  #ifdef EDITOR_VISUALIZATION
    metaIN.VizUV = IN.vizUV;
    metaIN.LightCoord = IN.lightCoord;
  #endif
  return UnityMetaFragment(metaIN);
#else
  #if defined(UNITY_PASS_FORWARDBASE) || defined(UNITY_PASS_FORWARDADD)
    UNITY_LIGHT_ATTENUATION(atten, IN, worldPos)
    fixed4 c = 0;
  #else
    half atten = 1;
  #endif

  float3 worldN;
  worldN.x = dot(_unity_tbn_0, o.Normal);
  worldN.y = dot(_unity_tbn_1, o.Normal);
  worldN.z = dot(_unity_tbn_2, o.Normal);
  worldN = normalize(worldN);
  o.Normal = worldN;

  // Setup lighting environment
  UnityGI gi;
  UNITY_INITIALIZE_OUTPUT(UnityGI, gi);
  gi.indirect.diffuse = 0;
  gi.indirect.specular = 0;

  #if defined(UNITY_PASS_FORWARDBASE) || defined(UNITY_PASS_FORWARDADD)
    gi.light.color = _LightColor0.rgb;
    gi.light.dir = lightDir;
  #elif defined(UNITY_PASS_DEFERRED)
    gi.light.color = 0;
    gi.light.dir = half3(0, 1, 0);
  #endif

  // Call GI (lightmaps/SH/reflections) lighting function
  #if defined(UNITY_PASS_FORWARDADD)
    gi.light.color *= atten;
  #endif
  #if defined(UNITY_PASS_FORWARDBASE) || defined(UNITY_PASS_DEFERRED)
    UnityGIInput giInput;
    UNITY_INITIALIZE_OUTPUT(UnityGIInput, giInput);
    giInput.light = gi.light;
    giInput.worldPos = worldPos;
    giInput.worldViewDir = worldViewDir;
    giInput.atten = atten;
    #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
      giInput.lightmapUV = IN.lmap;
    #else
      giInput.lightmapUV = 0.0;
    #endif
    #if UNITY_SHOULD_SAMPLE_SH && !UNITY_SAMPLE_FULL_SH_PER_PIXEL
      giInput.ambient = IN.sh;
    #else
      giInput.ambient.rgb = 0.0;
    #endif
    giInput.probeHDR[0] = unity_SpecCube0_HDR;
    giInput.probeHDR[1] = unity_SpecCube1_HDR;
    #if defined(UNITY_SPECCUBE_BLENDING) || defined(UNITY_SPECCUBE_BOX_PROJECTION)
      giInput.boxMin[0] = unity_SpecCube0_BoxMin; // .w holds lerp value for blending
    #endif
    #ifdef UNITY_SPECCUBE_BOX_PROJECTION
      giInput.boxMax[0] = unity_SpecCube0_BoxMax;
      giInput.probePosition[0] = unity_SpecCube0_ProbePosition;
      giInput.boxMax[1] = unity_SpecCube1_BoxMax;
      giInput.boxMin[1] = unity_SpecCube1_BoxMin;
      giInput.probePosition[1] = unity_SpecCube1_ProbePosition;
    #endif
    LightingStandard_GI(o, giInput, gi);
  #endif

  #if defined(UNITY_PASS_FORWARDBASE) || defined(UNITY_PASS_FORWARDADD)
    // realtime lighting: call lighting function
    c += LightingStandard (o, worldViewDir, gi);
    #if defined(UNITY_PASS_FORWARDADD)
      c.a = 0.0;
    #endif
    UNITY_APPLY_FOG(_unity_fogCoord, c); // apply fog
    UNITY_OPAQUE_ALPHA(c.a);
    return c;
  #elif defined(UNITY_PASS_DEFERRED)
    // call lighting function to output g-buffer
    outEmission = LightingStandard_Deferred (o, worldViewDir, gi, outGBuffer0, outGBuffer1, outGBuffer2);
    #if defined(SHADOWS_SHADOWMASK) && (UNITY_ALLOWED_MRT_COUNT > 4)
      outShadowMask = UnityGetRawBakedOcclusions (IN.lmap.xy, worldPos);
    #endif
    #ifndef UNITY_HDR_ON
    outEmission.rgb = exp2(-outEmission.rgb);
    #endif
  #endif

#endif
}

#endif // STANDARDFLAT
