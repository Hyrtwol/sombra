using UnityEngine;

namespace Sombra
{
    public class MaterialShowcase : MonoBehaviour
    {
        public Mesh Mesh;
        public Vector3 ColumnStart;
        public Vector3 ColumnOffset;
        public Vector3 RowStart;
        public Vector3 RowOffset;
    }
}
