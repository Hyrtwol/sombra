Shader "Cauldron/Unlit/Cubemap"
{
	Properties
	{
		_Color("Color", Color) = (1, 1, 1, 1)
		[NoScaleOffset] _MainTex("Albedo (RGB CUBE)", CUBE) = "white" {}
		[KeywordEnum(VertexLookup, NormalLookup, UVLookup)] _LookupFunc("Lookup function", Float) = 0
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile __ _LOOKUPFUNC_NORMALLOOKUP _LOOKUPFUNC_UVLOOKUP

			#include "UnityCG.cginc"
            #include "../CGIncludes/CauldronCubemap.cginc"

			UNITY_DECLARE_TEXCUBE(_MainTex);
			fixed4 _Color;

			struct appdata_t
			{
				float4 vertex  : POSITION;
				LOOKUP_APPDATA
			};

			struct v2f
			{
				float4 vertex  : SV_POSITION;
			    LOOKUP_DECLARE : TEXCOORD0;
			};

			v2f vert(appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				LOOKUP_TRANSFER(o, v);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
			    return LOOKUP_SAMPLE_TEXCUBE(_MainTex, i) * _Color;
			}

			ENDCG
		}
	}
}
