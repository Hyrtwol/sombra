using UnityEditor;
using UnityEngine;
//using TargetAttributes = UnityEditor.BuildTargetDiscovery.TargetAttributes;

namespace Sombra
{
    public class SombraShaderGUI : ShaderGUI
    {
        private static class Styles
        {
            public static string advancedText = "Advanced Options";
        }

        MaterialEditor m_MaterialEditor;

        public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] props)
        {
            m_MaterialEditor = materialEditor;
            Material material = materialEditor.target as Material;
            //MaterialProperty materialProperty = ShaderGUI.FindProperty("_Metallic", props);
            //SunDiskMode sunDiskMode = (SunDiskMode)materialProperty.floatValue;
            float labelWidth = EditorGUIUtility.labelWidth;
            for (int i = 0; i < props.Length; i++)
            {
                MaterialProperty prop = props[i];
                if (prop.type == MaterialProperty.PropType.Float)
                {
                    EditorGUIUtility.labelWidth = labelWidth;
                }
                else
                {
                    materialEditor.SetDefaultGUIWidths();
                }
                if ((prop.flags & MaterialProperty.PropFlags.HideInInspector) == 0
                    //&& (!(mp.name == "_SunSizeConvergence") || sunDiskMode == SunDiskMode.HighQuality)
                    )
                {
                    float propertyHeight = materialEditor.GetPropertyHeight(prop, prop.displayName);
                    Rect controlRect = EditorGUILayout.GetControlRect(true, propertyHeight, EditorStyles.layerMaskField);
                    materialEditor.ShaderProperty(controlRect, prop, prop.displayName);
                }
            }
            //EditorGUIUtility.labelWidth = labelWidth;

            m_MaterialEditor.SetDefaultGUIWidths();

            EditorGUILayout.Space();
            GUILayout.Label(Styles.advancedText, EditorStyles.boldLabel);
            m_MaterialEditor.RenderQueueField();

            m_MaterialEditor.EnableInstancingField();
            m_MaterialEditor.DoubleSidedGIField();
        }
    }
}
