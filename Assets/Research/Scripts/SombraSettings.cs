#if NOT_YET
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Sombra
{
    [System.Serializable]
    public class SombraSettings : ScriptableObject
    {
        [Header("Scenes")]
        public SceneItem MainScene;
        public SceneItem QuitScene;
        public SceneItem OptionsScene;
        public SceneItem GameScene;

        [Header("Information")]
        public string Description;

        [Header("Input")]
        public KeyCode ExitSceneKeyCode = KeyCode.Escape;
        public KeyCode CameraToggleKey = KeyCode.Space;

        public KeyCode UpdateLight = KeyCode.L;
        public KeyCode FindPath = KeyCode.P;

        [Header("Blocks")]
        public Sprite[] BlockSprites;
        public GameObject[] BlockMeshes;

#if UNITY_EDITOR
        public void OnValidate()
        {
            MainScene.Validate(this);
            QuitScene.Validate(this);
            OptionsScene.Validate(this);
            GameScene.Validate(this);

            ScriptableObject cauldronSettings = null;
            var guid = AssetDatabase.FindAssets("t:PlayerArtifact").FirstOrDefault();
            if (!string.IsNullOrEmpty(guid))
            {
                var assetPath = AssetDatabase.GUIDToAssetPath(guid);
                cauldronSettings = AssetDatabase.LoadAssetAtPath<ScriptableObject>(assetPath);
            }
            if (cauldronSettings != null)
            {
                var so = new SerializedObject(cauldronSettings);
                var po = so.FindProperty("Description");
                if (po != null && (po.stringValue != Description))
                {
                    Debug.Log("Settings Description to " + po.stringValue + " on" + this);
                    Description = po.stringValue;
                }
            }
        }
#endif

        [System.Serializable]
        public class SceneItem
        {
            public string Label;
            [HideInInspector]
            public string SceneName;
#if UNITY_EDITOR
            public SceneAsset Scene;
#endif

#if UNITY_EDITOR
            public void Validate(Object target)
            {
                if (Scene != null)
                {
                    if (SceneName != Scene.name)
                    {
                        Debug.Log("Settings scene name to \"" + Scene.name + "\" on " + this + " was \"" + SceneName + "\"");
                        SceneName = Scene.name;
                        EditorUtility.SetDirty(target);
                    }
                }
                //else
                //{
                //    SceneName = null;
                //}
                if (string.IsNullOrEmpty(Label))
                {
                    Debug.Log("Settings label to \"" + SceneName + "\" on " + this);
                    Label = SceneName;
                    EditorUtility.SetDirty(target);
                }
            }
#endif
        }
    }
}
#endif
