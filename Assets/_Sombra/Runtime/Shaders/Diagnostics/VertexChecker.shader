Shader "Cauldron/Diagnostics/VertexChecker"
{
	Properties
	{
		[KeywordEnum(Normal, Tangent, UV0, UV1, UV2, UV3)] _Channel("Vertex channel", Int) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _CHANNEL_NORMAL _CHANNEL_TANGENT _CHANNEL_UV0 _CHANNEL_UV1 _CHANNEL_UV2 _CHANNEL_UV3

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				#if _CHANNEL_NORMAL
					float3 normal : NORMAL;
				#elif _CHANNEL_TANGENT
					float4 tangent : TANGENT;
				#elif _CHANNEL_UV0
					float3 uv : TEXCOORD0;
				#elif _CHANNEL_UV1
					float3 uv : TEXCOORD1;
				#elif _CHANNEL_UV2
					float3 uv : TEXCOORD2;
				#elif _CHANNEL_UV3
					float3 uv : TEXCOORD3;
				#endif
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 value : TEXCOORD0;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				#if _CHANNEL_NORMAL
					o.value = v.normal;
				#elif _CHANNEL_TANGENT
					o.value = v.tangent.xyz * v.tangent.w;
				#else
					o.value = v.uv;
				#endif
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				float3 col = i.value;
				#if _CHANNEL_NORMAL || _CHANNEL_TANGENT
					col = normalize(col) * 0.5 + 0.5;
				#endif
				return fixed4(col, 0.0);
			}
			ENDCG
		}
	}
}
