#if NOT_YET
using UnityEditor;

namespace Sombra.Editor
{
    [InitializeOnLoad]
    public static class SombraSettingsEditor
    {
        [MenuItem(ItemNamePrefix + "/" + nameof(Create_Settings))]
        public static void Create_Settings()
        {
            var settings = GetGameSettings();
            if (settings == null)
            {
                var type = typeof(SombraSettings);
                string assetPath = EditorUtility.SaveFilePanelInProject($"Save {type.Name}", Application.productName, "asset", "", "Assets");
                if (string.IsNullOrEmpty(assetPath)) return;
                Debug.Log(assetPath);

                settings = ScriptableObject.CreateInstance<SombraSettings>();
                settings.name = type.Name;
                AssetDatabase.CreateAsset(settings, assetPath);
                AssetDatabase.SaveAssets();
                Debug.Log("Created " + settings);
            }
            else
            {
                Debug.Log("Loaded " + settings);
            }
        }

        private static SombraSettings GetGameSettings()
        {
            string filter = $"t:{nameof(SombraSettings)}";
            string guid = AssetDatabase.FindAssets(filter).FirstOrDefault();
            if (string.IsNullOrEmpty(guid)) return null;
            string assetPath = AssetDatabase.GUIDToAssetPath(guid);
            return AssetDatabase.LoadAssetAtPath<SombraSettings>(assetPath);
        }
    }
}
#endif
