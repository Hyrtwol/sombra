#if !defined(VERTEXTILESGEOMETRY_INCLUDED)
#define VERTEXTILESGEOMETRY_INCLUDED

#ifdef HAVE_WORLD_POS
#define G2F_TRANSFER_WORLD_POS(o, i) o.worldPos = i.worldPos;
#else
#define G2F_TRANSFER_WORLD_POS(o, i)
#endif

#ifdef HAVE_WORLD_NML
#define G2F_TRANSFER_WORLD_NML(o, i) o.worldNormal = i.worldNormal;
#else
#define G2F_TRANSFER_WORLD_NML(o, i)
#endif

#ifdef HAVE_SH
#define G2F_TRANSFER_SH(o, i) o.sh = i.sh;
#else
#define G2F_TRANSFER_SH(o, i)
#endif

#ifdef HAVE_LMAP
#define G2F_TRANSFER_LMAP(o, i) o.lmap = i.lmap;
#else
#define G2F_TRANSFER_LMAP(o, i)
#endif

#ifdef HAVE_LMAP_FADEPOS
#define G2F_TRANSFER_LMAPFADEPOS(o, i) o.lmapFadePos = i.lmapFadePos;
#else
#define G2F_TRANSFER_LMAPFADEPOS(o, i)
#endif

#ifdef HAVE_LIGHT
#define G2F_TRANSFER_LIGHT(o, i) o._LightCoord = i._LightCoord;
#else
#define G2F_TRANSFER_LIGHT(o, i)
#endif

#ifdef HAVE_SHADOW
#define G2F_TRANSFER_SHADOW(o, i) o._ShadowCoord = i._ShadowCoord;
#else
#define G2F_TRANSFER_SHADOW(o, i)
#endif

#ifdef HAVE_FOG
#define G2F_TRANSFER_FOG(o, i) o.fogCoord = i.fogCoord;
#else
#define G2F_TRANSFER_FOG(o, i)
#endif

#ifdef HAVE_TSPACE
#define G2F_TRANSFER_TSPACE(o, i) o.tSpace0 = i.tSpace0; o.tSpace1 = i.tSpace1; o.tSpace2 = i.tSpace2;
#else
#define G2F_TRANSFER_TSPACE(o, i)
#endif

#ifdef HAVE_VIEWDIR
#define G2F_TRANSFER_VIEWDIR(o, i) o.viewDir = i.viewDir;
#else
#define G2F_TRANSFER_VIEWDIR(o, i)
#endif

#if EDITOR_VISUALIZATION
#define G2F_TRANSFER_VIS(o, i) o.vizUV = i.vizUV;
#else
#define G2F_TRANSFER_VIS(o, i)
#endif

#define G2F_DATATRANSFER(o /*g2f_surf_trio*/, i /*v2g_surf_trio*/) \
    o.pos = i.pos; \
    G2F_TRANSFER_WORLD_POS(o, i) \
    G2F_TRANSFER_WORLD_NML(o, i) \
    G2F_TRANSFER_SH(o, i) \
    G2F_TRANSFER_LMAP(o, i) \
    G2F_TRANSFER_LIGHT(o, i) \
    G2F_TRANSFER_SHADOW(o, i) \
    G2F_TRANSFER_FOG(o, i) \
    G2F_TRANSFER_TSPACE(o, i) \
    G2F_TRANSFER_VIS(o, i) \
    G2F_TRANSFER_VIEWDIR(o, i)

// geometry shader
[maxvertexcount(3)]
void geom_surf_trio(triangle v2g_surf_trio input[3], inout TriangleStream<g2f_surf_trio> triStream)
{
    //     1      0-1-2
    //    / \     1-2-0
    //   0---2    2-0-1

    VERTEX_POS(0, 1)
    VERTEX_POS(1, 2)
    VERTEX_POS(2, 0)

    {
        float3x3 tbn;
        VERTEX_TBN(0, 1, 2)
        VERTEX_TBN(1, 2, 0)
        VERTEX_TBN(2, 0, 1)
    }

    float3 texidx = GET_TEXTURE_INDEX_TRIO(input);

    float2 texofsX = GET_TEXTURE_OFFSET(input[0]);
    float2 texofsY = GET_TEXTURE_OFFSET(input[1]);
    float2 texofsZ = GET_TEXTURE_OFFSET(input[2]);

    g2f_surf_trio o;
    {
        G2F_DATATRANSFER(o, input[0])
        TRIO_UV_TRANSFER(o, 1, 0, 0, texidx, texofsX, texofsY + v10.xz, texofsZ + v20.xz)
        triStream.Append(o);
    }
    {
        G2F_DATATRANSFER(o, input[1])
        TRIO_UV_TRANSFER(o, 0, 1, 0, texidx, texofsX + v01.xz, texofsY, texofsZ + v21.xz)
        triStream.Append(o);
    }
    {
        G2F_DATATRANSFER(o, input[2])
        TRIO_UV_TRANSFER(o, 0, 0, 1, texidx, texofsX + v02.xz, texofsY + v12.xz, texofsZ)
        triStream.Append(o);
    }

}

#endif // VERTEXTILESGEOMETRY_INCLUDED
