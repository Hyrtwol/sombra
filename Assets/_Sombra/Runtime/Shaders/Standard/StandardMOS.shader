Shader "Cauldron/Standard/StandardMOS"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGBA)", 2D) = "white" {}

        [NoScaleOffset] _PackedTex("Packed MOS (RGB)", 2D) = "black" {}

        _GlossMapScale("Smoothness Scale", Range(0.0, 1.0)) = 1.0
        _OcclusionStrength("Occlusion Strength", Range(0.0, 1.0)) = 1.0
    }
    CGINCLUDE
    #define _PACKING_RGBA_MOS
    ENDCG
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200
        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows
        #pragma target 3.0
        //#define _PACKING_RGBA_MOS
        #include "StandardPacked.cginc"
        ENDCG
    }
    FallBack "Diffuse"
    CustomEditor "Sombra.SombraShaderGUI"
}
