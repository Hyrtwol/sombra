#if !defined(STANDARD_PACKED)
#define STANDARD_PACKED

#include "../CGIncludes/PackedProperties.cginc"

// samplers

UNITY_DECLARE_TEX2D(_MainTex);
#ifdef PACKED_TEX
UNITY_DECLARE_TEX2D(_PackedTex);
#endif
#ifdef _NORMALMAP
UNITY_DECLARE_TEX2D(_BumpMap);
#endif

struct Input {
	float2 uv_MainTex;
};

UNITY_INSTANCING_BUFFER_START(Props)
UNITY_INSTANCING_BUFFER_END(Props)

void surf(Input IN, inout SurfaceOutputStandard o)
{
	fixed4 color = UNITY_SAMPLE_TEX2D(_MainTex, IN.uv_MainTex);
#if defined(PACKED_TEX)
	fixed4 packed = UNITY_SAMPLE_TEX2D(_PackedTex, IN.uv_MainTex);
#endif
	TRANSFER_PACKED(o)

#ifdef _NORMALMAP
	half4 packednormal = UNITY_SAMPLE_TEX2D(_BumpMap, IN.uv_MainTex);
#ifdef _NORMALMAP_SCALE
	o.Normal = UnpackScaleNormal(packednormal, _BumpScale);
#else
	o.Normal = UnpackNormal(packednormal);
#endif // _NORMALMAP_SCALE
#endif // _NORMALMAP
}

#endif // STANDARD_PACKED
