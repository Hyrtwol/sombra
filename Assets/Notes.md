# Sombra Shader Notes

https://docs.unity3d.com/Manual/SL-MultipleProgramVariants.html
```
#pragma multi_compile_local _PACKING_RGBA _PACKING_RGBS _PACKING_RGBA_MOS _PACKING_RGBS_MOXA
#pragma shader_feature_local _PACKING_RGBS _PACKING_RGBA_MOS _PACKING_RGBS_MOXA
#pragma shader_feature_local _ _PACKING_RGBS _PACKING_RGBA_MOS _PACKING_RGBS_MOXA
```

```
UNITY_PASS_FORWARDBASE   Forward rendering base pass (main directional light, lightmaps, SH).
UNITY_PASS_FORWARDADD    Forward rendering additive pass (one light per pass).
UNITY_PASS_DEFERRED      Deferred shadingpass (renders g buffer).
UNITY_PASS_SHADOWCASTER  Shadow caster and depth Texture rendering pass.
UNITY_PASS_PREPASSBASE   Legacy deferred lighting base pass (renders normals and specular exponent).
UNITY_PASS_PREPASSFINAL  Legacy deferred lighting final pass (applies lighting and Textures).
```

```
struct appdata_base {
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float4 texcoord : TEXCOORD0;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct appdata_tan {
    float4 vertex : POSITION;
    float4 tangent : TANGENT;
    float3 normal : NORMAL;
    float4 texcoord : TEXCOORD0;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct appdata_full {
    float4 vertex : POSITION;
    float4 tangent : TANGENT;
    float3 normal : NORMAL;
    float4 texcoord : TEXCOORD0;
    float4 texcoord1 : TEXCOORD1;
    float4 texcoord2 : TEXCOORD2;
    float4 texcoord3 : TEXCOORD3;
    fixed4 color : COLOR;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};
```
