Shader "Cauldron/Unlit/TextureChannelColor"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        [Enum(X,0,Y,1,Z,2,W,3)] _TexChannel("Texture Channel", Int) = 0

		_LoColor ("Low Color", Color) = (0,0,0,0)
		_HiColor ("Hi Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            int _TexChannel;
            fixed4 _LoColor, _HiColor;

            #define TRANSFORM_COLOR(col) lerp(_LoColor, _HiColor, col[_TexChannel])

            #include "TextureChannelMixer.cginc"

            ENDCG
        }
    }
}
