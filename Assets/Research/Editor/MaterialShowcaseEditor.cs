using System.Text;
using UnityEditor;
using UnityEngine;

namespace Sombra
{
    [CustomEditor(typeof(MaterialShowcase))]
    public class MaterialShowcaseEditor : UnityEditor.Editor
    {
        protected static readonly GUILayoutOption _btnWidth = GUILayout.Width(140f);

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var showcase = target as MaterialShowcase;
            if (showcase == null) return;

            int mode = 0;
            GUILayout.BeginHorizontal();
            if (GUILayout.Button(nameof(Scan), _btnWidth)) mode = 1;
            if (GUILayout.Button(nameof(Rename), _btnWidth)) mode = 2;
            if (GUILayout.Button(nameof(Layout), _btnWidth)) mode = 3;
            GUILayout.EndHorizontal();

            switch (mode)
            {
                case 1: Scan(showcase); break;
                case 2: Rename(showcase); break;
                case 3: Layout(showcase); break;
                default:
                    if (mode > 0) Debug.Log($"{nameof(mode)}={mode}");
                    break;
            }
        }

        private void Scan(MaterialShowcase showcase)
        {
            var sb = new StringBuilder();

            var mrs = showcase.GetComponentsInChildren<MeshRenderer>(true);
            foreach (var mr in mrs)
            {
                sb.AppendLine($"{mr.name} --> {mr.sharedMaterial.name}");
            }

            Debug.Log(sb.ToString());
        }

        private void Rename(MaterialShowcase showcase)
        {
            var sb = new StringBuilder();

            foreach (var mr in showcase.GetComponentsInChildren<MeshRenderer>(true))
            {
                sb.AppendLine($"{mr.name} = {mr.sharedMaterial.name}");
                mr.name = mr.sharedMaterial.name;
            }
            if (showcase.Mesh != null)
                foreach (var mf in showcase.GetComponentsInChildren<MeshFilter>(true))
                {
                    sb.AppendLine($"{mf.name} = {mf.sharedMesh.name}");
                    mf.sharedMesh = showcase.Mesh;
                }

            Debug.Log(sb.ToString());
        }

        private void Layout(MaterialShowcase showcase)
        {
            var sb = new StringBuilder();

            var tr = showcase.transform;

            var cp = showcase.ColumnStart;
            var co = showcase.ColumnOffset;
            for (int ci = 0; ci < tr.childCount; ci++)
            {
                var ch = tr.GetChild(ci);
                var cc = ch.childCount;
                sb.AppendLine($"{ch.name}.position = {cp} childCount = {cc}");
                ch.transform.localPosition = cp;

                var rp = showcase.RowStart;
                var ro = showcase.RowOffset;
                for (int ri = 0; ri < cc; ri++)
                {
                    var rh = ch.GetChild(ri);
                    sb.AppendLine($"{rh.name}.position = {rp}");
                    rh.transform.localPosition = rp;

                    rp += ro;
                }

                cp += co;
            }

            Debug.Log(sb.ToString());
        }
    }
}
