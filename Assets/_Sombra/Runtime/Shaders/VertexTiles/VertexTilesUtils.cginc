#ifndef VERTEX_TILES_UTILS_INCLUDED
#define VERTEX_TILES_UTILS_INCLUDED

half _TexScale, _TexBlend;

#define GET_APPDATA_TPOS(v) v.vertex * _TexScale
#define GET_APPDATA_TNORMAL(v) v.normal
//#define GET_APPDATA_TNORMAL(v) normalize(v.normal)
#define GET_APPDATA_TPARAM(v) v.texcoord
#define GET_APPDATA_TTANGENT(v) v.texcoord3

#define VT_V2G_TDATA(i0, i1, i2, i3) \
    float4 tparam    : TEXCOORD##i0; \
    float3 tpos      : TEXCOORD##i1; \
    float3 tnormal   : TEXCOORD##i3; \
    float3 ttangent  : TEXCOORD##i2;

#define VT_TRANSFER_APPDATA(v /*appdata*/, o /*v2g_surf_trio*/) \
    o.tparam   = GET_APPDATA_TPARAM(v); \
    o.tpos     = GET_APPDATA_TPOS(v); \
    o.tnormal  = GET_APPDATA_TNORMAL(v); \
    o.ttangent = GET_APPDATA_TTANGENT(v);


#define VT_G2F_TDATA(i0, i1, i2, i3) \
    float3 uvX       : TEXCOORD##i0; \
    float3 uvY       : TEXCOORD##i1; \
    float3 uvZ       : TEXCOORD##i2; \
    float3 uvWeights : TEXCOORD##i3;

#define VT_G2F_TDATA_PACKED(i0, i1, i2) \
    float4 uvX       : TEXCOORD##i0; \
    float4 uvY       : TEXCOORD##i1; \
    float4 uvZ       : TEXCOORD##i2;

#ifdef TRIO_PACKUV
#define TRIO_UV_TRANSFER(o,tx,ty,tz,ti,uvx,uvy,uvz) \
    o.uvX = float4(uvx, ti.x, tx); \
    o.uvY = float4(uvy, ti.y, ty); \
    o.uvZ = float4(uvz, ti.z, tz);
#else
#define TRIO_UV_TRANSFER(o,tx,ty,tz,ti,uvx,uvy,uvz) \
    o.uvX = float3(uvx, ti.x); \
    o.uvY = float3(uvy, ti.y); \
    o.uvZ = float3(uvz, ti.z); \
    o.uvWeights = float3(tx, ty, tz);
#endif


#define GET_TEXTURE_INDEX(v) v.tparam.x
#define GET_TEXTURE_SCALE(v) v.tparam.y
#define GET_TEXTURE_OFFSET(v) v.tparam.zw

#define GET_TEXTURE_INDEX_TRIO(input) float3(GET_TEXTURE_INDEX(input[0]), GET_TEXTURE_INDEX(input[1]), GET_TEXTURE_INDEX(input[2]))
#define GET_TEXTURE_SCALE_TRIO(input) float3(GET_TEXTURE_SCALE(input[0]), GET_TEXTURE_SCALE(input[1]), GET_TEXTURE_SCALE(input[2]))


#define CREATE_TBN(tangent, normal) float3x3(tangent, normal, /*binormal*/ cross(normal, tangent))

#define APPLY_TBN(vtx, v1, v2) \
    tbn = CREATE_TBN(vtx.ttangent, vtx.tnormal); \
    v1 = mul(tbn, v1); \
    v2 = mul(tbn, v2);

//#define APPLY_TBN(vtx, v1, v2) \
//    tbn = CREATE_TBN(vtx.ttangent, vtx.tnormal); \
//    v1 = mul(tbn, v1) * GET_TEXTURE_SCALE(vtx); \
//    v2 = mul(tbn, v2) * GET_TEXTURE_SCALE(vtx);

#define VERTEX_POS(i1,i2) float3 v##i1##i2 = input[i2].tpos - input[i1].tpos, v##i2##i1 = -v##i1##i2;
#define VERTEX_TBN(i0,i1,i2) APPLY_TBN(input[i0], v##i0##i1, v##i0##i2)


#ifdef TRIO_PACKUV
#define GET_UV_WEIGHTS(i) float3(i.uvX.w, i.uvY.w, i.uvZ.w)
#else
#define GET_UV_WEIGHTS(i) i.uvWeights
#endif

// Tighten up the blending zone and force weights to sum to 1.0 (very important!)
#define VERTEX_TILES_BLENDWEIGHTS(blendWeights) \
	blendWeights = pow(blendWeights, _TexBlend); \
	blendWeights /= (blendWeights.x + blendWeights.y + blendWeights.z).xxx;

inline float3 CalcBlendWeights(in float3 blendWeights)
{
    VERTEX_TILES_BLENDWEIGHTS(blendWeights)
	return blendWeights;
}

//#define GET_BLENDWEIGHTS_WEIGHTED(i, bw) \
//	bw = GET_UV_WEIGHTS(i); \
//	VERTEX_TILES_BLENDWEIGHTS(bw)

#define CALC_BLEND_WEIGHTS(i) CalcBlendWeights(GET_UV_WEIGHTS(i))

// TRIO_SAMPLE_TEX2DARRAY

#define SAMPLE_TEX2DARRAY_WEIGHTED(t, uvx, uvy, uvz, bw) \
	UNITY_SAMPLE_TEX2DARRAY(t, uvx) * bw.x + \
	UNITY_SAMPLE_TEX2DARRAY(t, uvy) * bw.y + \
	UNITY_SAMPLE_TEX2DARRAY(t, uvz) * bw.z

#define TRIO_SAMPLE_TEX2DARRAY_WEIGHTED(t, i, bw) SAMPLE_TEX2DARRAY_WEIGHTED(t, i.uvX.xyz, i.uvY.xyz, i.uvZ.xyz, bw)

#endif // VERTEX_TILES_UTILS_INCLUDED
