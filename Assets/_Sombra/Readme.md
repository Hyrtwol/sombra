# Sombra Shader Notes

A collection of Unity shaders.

## Unity notes

    struct SurfaceOutputStandard
    {
        fixed3 Albedo;       // base (diffuse or specular) color
        float3 Normal;       // tangent space normal, if written
        half3  Emission;
        half   Metallic;     // 0=non-metal, 1=metal
        half   Smoothness;   // 0=rough, 1=smooth
        half   Occlusion;    // occlusion (default 1)
        fixed  Alpha;        // alpha for transparencies
    };
