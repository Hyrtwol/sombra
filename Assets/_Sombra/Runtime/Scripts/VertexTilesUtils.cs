using UnityEngine;

namespace Sombra
{
    public static class VertexTilesUtils
    {
        public static Vector3 MakeRandomTangent(/*this System.Random rng,*/ ref Vector3 n)
        {
            Vector3 t;
            do
            {
                //t = Cauldron.Unity.RandomExtensions.NextNormal3(rng);
                t = Random.onUnitSphere;
            } while (System.Math.Abs(Vector3.Dot(n, t)) > 0.9);
            t = Vector3.Cross(t, n);
            t.Normalize();
            return t;
        }
    }
}
