Shader "Cauldron/Unlit/Texture2DArray"
{
    Properties
    {
        [IntRange] _TexIdx("Texture Index", Range(0, 63)) = 0
        _MainTex("Albedo (RGB 2DArray)", 2DArray) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            UNITY_DECLARE_TEX2DARRAY(_MainTex);
            float4 _MainTex_ST;
            int _TexIdx;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 uvi = float3(i.uv, _TexIdx);
                fixed4 col = UNITY_SAMPLE_TEX2DARRAY(_MainTex, uvi);
                return col;
            }
            ENDCG
        }
    }
}
