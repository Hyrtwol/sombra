#if !defined(CAULDRON_CUBEMAP_INCLUDED)
#define CAULDRON_CUBEMAP_INCLUDED

// Lookup function: VertexLookup, NormalLookup, UVLookup

#if _LOOKUPFUNC_NORMALLOOKUP

#define LOOKUP_APPDATA float3 normal : NORMAL;
#define LOOKUP(v) v.normal;

#elif _LOOKUPFUNC_UVLOOKUP

#define LOOKUP_APPDATA float3 texcoord : TEXCOORD0;
#define LOOKUP(v) v.texcoord.xyz;

#else

#define LOOKUP_APPDATA
#define LOOKUP(v) v.vertex.xyz;

#endif

#define LOOKUP_DECLARE float3 lookup
#define LOOKUP_TRANSFER(o, v) o.lookup = LOOKUP(v);

#define LOOKUP_SAMPLE_TEXCUBE(tex, v) UNITY_SAMPLE_TEXCUBE(tex, normalize(v.lookup))

#endif // CAULDRON_CUBEMAP_INCLUDED
