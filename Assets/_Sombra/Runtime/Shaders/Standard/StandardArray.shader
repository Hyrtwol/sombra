Shader "Cauldron/Standard/StandardArray"
{
    Properties
    {
        [IntRange] _TexIdx ("Texture Index", Range(0, 63)) = 0

        _Color ("Color", Color) = (1,1,1,1)
        _MainTex("Albedo (RGB 2DArray)", 2DArray) = "" {}

        [NoScaleOffset] _PackedTex("Packed (RGBA 2DArray)", 2DArray) = "black" {}
        [KeywordEnum(RGBA, RGBS, RGBA_MOS, RGBS_MOXA)] _Packing("Packing", Int) = 0

        _Metallic ("Metallic", Range(0,1)) = 0.0
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _GlossMapScale("Smoothness Scale", Range(0.0, 1.0)) = 1.0
        _OcclusionStrength("Occlusion", Range(0.0, 1.0)) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM

        #pragma require 2darray
        #pragma surface surf Standard fullforwardshadows
        #pragma target 3.0
        #pragma shader_feature_local _ _PACKING_RGBS _PACKING_RGBA_MOS _PACKING_RGBS_MOXA

        #include "StandardArray.cginc"

        ENDCG
    }
    FallBack "Diffuse"
    CustomEditor "Sombra.SombraShaderGUI"
}
