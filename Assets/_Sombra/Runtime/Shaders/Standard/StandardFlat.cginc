#if !defined(STANDARD_FLAT_GEOMETRY)
#define STANDARD_FLAT_GEOMETRY

#if defined(UNITY_PASS_FORWARDBASE)
#define GeomVertexInput VertexOutputForwardBase
#elif defined(UNITY_PASS_DEFERRED)
#define GeomVertexInput VertexOutputDeferred
#endif
#if !defined(GeomVertexOutput)
#define GeomVertexOutput GeomVertexInput
#endif

//#define GET_TANGENT(IN) float3(IN.tangentToWorldAndPackedData.x, IN.tangentToWorldAndPackedData.x, IN.tangentToWorldAndPackedData.x)
//#define GET_NORMAL(IN) float3(IN.tangentToWorldAndPackedData.z, IN.tangentToWorldAndPackedData.z, IN.tangentToWorldAndPackedData.z)
//#define GET_WORLDPOS(IN) float3(IN.tangentToWorldAndPackedData.w, IN.tangentToWorldAndPackedData.w, IN.tangentToWorldAndPackedData.w)

// geometry shader
[maxvertexcount(3)]
void geom(triangle GeomVertexOutput input[3], inout TriangleStream<GeomVertexInput> triStream)
{
    //     1
    //    / \
    //   0---2
    float3 worldPos0 = IN_WORLDPOS(input[0]);
    float3 worldPos1 = IN_WORLDPOS(input[1]);
    float3 worldPos2 = IN_WORLDPOS(input[2]);

    float3 normalWorld = normalize(
        cross(worldPos1-worldPos0, worldPos2-worldPos0)
    );

    input[0].tangentToWorldAndPackedData[2].xyz = normalWorld;
    input[1].tangentToWorldAndPackedData[2].xyz = normalWorld;
    input[2].tangentToWorldAndPackedData[2].xyz = normalWorld;

    triStream.Append(input[0]);
    triStream.Append(input[1]);
    triStream.Append(input[2]);
}

#endif // STANDARD_FLAT_GEOMETRY
