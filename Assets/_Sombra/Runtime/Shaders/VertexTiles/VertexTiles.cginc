#if !defined(VERTEXTILES_INCLUDED)
#define VERTEXTILES_INCLUDED

// UNITY_PASS_FORWARDBASE   Forward rendering base pass (main directional light, lightmaps, SH).
// UNITY_PASS_FORWARDADD    Forward rendering additive pass (one light per pass).
// UNITY_PASS_DEFERRED      Deferred shadingpass (renders g buffer).
// UNITY_PASS_SHADOWCASTER  Shadow caster and depth Texture rendering pass.
// UNITY_PASS_PREPASSBASE   Legacy deferred lighting base pass (renders normals and specular exponent).
// UNITY_PASS_PREPASSFINAL  Legacy deferred lighting final pass (applies lighting and Textures).

#include "../CGIncludes/CauldronCommon.cginc"
//#include "../CGIncludes/TrioTileingData.cginc"
#include "../CGIncludes/PackedProperties.cginc"

UNITY_DECLARE_TEX2DARRAY(_MainTex);
#ifdef PACKED_TEX
UNITY_DECLARE_TEX2DARRAY(_PackedTex);
#endif
//half _TexScale, _TexBlend;

#define TRIO_PACKUV

#include "VertexTilesUtils.cginc"

#if 1
struct appdata
{
    float4 vertex    : POSITION;
    //float4 tangent   : TANGENT;
    float3 normal    : NORMAL;
    float4 texcoord  : TEXCOORD0; // x = tex-index, y = tex-scale, zw = tex-offset
    float4 texcoord1 : TEXCOORD1; // lightmap
    float4 texcoord2 : TEXCOORD2; // dynlightmap
    float4 texcoord3 : TEXCOORD3; // xyz = tex-tangent, w = tex-index
    UNITY_VERTEX_INPUT_INSTANCE_ID
};
#else
#define appdata appdata_full
dont use yet
#endif

#define V2G_DATA \
    UNITY_POSITION(pos); \
    VT_V2G_TDATA(0,1,2,3) \
    CAULDRON_DECLARE_WORLD_POS_NORMAL(4,5)

#ifdef TRIO_PACKUV

#define G2F_DATA \
    UNITY_POSITION(pos); \
    VT_G2F_TDATA_PACKED(0,1,2) \
    CAULDRON_DECLARE_WORLD_POS_NORMAL(3,4)

#else

#define G2F_DATA \
    UNITY_POSITION(pos); \
    VT_G2F_TDATA(0,1,2,3) \
    CAULDRON_DECLARE_WORLD_POS_NORMAL(4,5)

#endif

#define HAVE_WORLD_POS
#define HAVE_WORLD_NML

UNITY_INSTANCING_BUFFER_START(Props)
UNITY_INSTANCING_BUFFER_END(Props)

#define TRIO_SAMPLE_MAINTEX(i) fixed4 color = TRIO_SAMPLE_TEX2DARRAY_WEIGHTED(_MainTex, i, blendWeights);
#ifdef PACKED_TEX
#define TRIO_SAMPLE_PACKED(i) fixed4 packed = TRIO_SAMPLE_TEX2DARRAY_WEIGHTED(_PackedTex, i, blendWeights);
#else
#define TRIO_SAMPLE_PACKED(i)
#endif

#define TRANSFER_SURFACEOUTPUTSTANDARD(i /*g2f_surf_trio*/, o /*SurfaceOutputStandard*/) \
    float3 blendWeights = CALC_BLEND_WEIGHTS(i); \
    TRIO_SAMPLE_MAINTEX(i) \
    TRIO_SAMPLE_PACKED(i) \
    TRANSFER_PACKED(o) \
    o.Emission   = 0.0;

#endif // VERTEXTILES_INCLUDED
