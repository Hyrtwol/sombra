Shader "Cauldron/Standard/Standard3D"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        [NoScaleOffset] _MainTex ("Albedo (RGB 3D)", 3D) = "white" {}
        _TexScale("TexScale", Range(0,256)) = 1.0
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0

        [Toggle(_SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A)] _SmoothnessTextureChannel("Smoothness Albedo Alpha", Float) = 0
        _GlossMapScale("Smoothness Scale", Range(0.0, 1.0)) = 1.0

        [KeywordEnum(WorldSpace, ObjectSpace)] _UVSource("UV Source", Int) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert
        #pragma multi_compile __ _UVSOURCE_OBJECTSPACE
        #pragma shader_feature_local_fragment _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
        #pragma target 3.0

        UNITY_DECLARE_TEX3D(_MainTex);

        fixed4 _Color;
        half _Glossiness, _GlossMapScale;
        half _Metallic;
        float _TexScale;

        struct Input
        {
            float3 lookup;
        };

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        void vert(inout appdata_full v, out Input o)
        {
#if _UVSOURCE_OBJECTSPACE
            o.lookup = v.vertex.xyz;
#else
            o.lookup = mul(unity_ObjectToWorld, v.vertex).xyz;
#endif
            o.lookup *= _TexScale;
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 c = UNITY_SAMPLE_TEX3D(_MainTex, IN.lookup);
            o.Albedo = c.rgb * _Color.rgb;
            o.Metallic = _Metallic;
#ifdef _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
            o.Smoothness = c.a * _GlossMapScale;
            o.Alpha = _Color.a;
#else
            o.Smoothness = _Glossiness;
            o.Alpha = c.a * _Color.a;
#endif
        }
        ENDCG
    }
    FallBack "Diffuse"
    CustomEditor "Sombra.SombraShaderGUI"
}
