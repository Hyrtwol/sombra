Shader "Cauldron/Unlit/TextureChannelMixer"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        [Enum(X,0,Y,1,Z,2,W,3)] _ChannelX ("Texture Channel X", Int) = 0
        [Enum(X,0,Y,1,Z,2,W,3)] _ChannelY ("Texture Channel Y", Int) = 1
        [Enum(X,0,Y,1,Z,2,W,3)] _ChannelZ ("Texture Channel Z", Int) = 2
        [Enum(X,0,Y,1,Z,2,W,3)] _ChannelW ("Texture Channel W", Int) = 3
        _Color ("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            int _ChannelX, _ChannelY, _ChannelZ, _ChannelW;
            fixed4 _Color;

            #define TRANSFORM_COLOR(col) float4(col[_ChannelX], col[_ChannelY], col[_ChannelZ], col[_ChannelW]) * _Color

            #include "TextureChannelMixer.cginc"

            ENDCG
        }
    }
}
