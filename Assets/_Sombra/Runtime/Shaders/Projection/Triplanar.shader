Shader "Cauldron/Projection/Triplanar"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTexX ("Albedo X (RGB)", 2D) = "white" {}
		_MainTexY ("Albedo Y (RGB)", 2D) = "white" {}
		_MainTexZ ("Albedo Z (RGB)", 2D) = "white" {}
		_Metallic("Metallic", Range(0,1)) = 0.0
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_TexScale("Texture Scale", Float) = 1.0
		[PowerSlider(4.0)] _TexBlend("Blend Sharpness", Range(1,64)) = 2.0
		[KeywordEnum(WorldSpace, ObjectSpace)] _UVSource("UV Source", Int) = 0
		[KeywordEnum(Trio, Duo, Single, TopBottom, Top)] _TexCnt("Texture Usage", Int) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows
		#pragma multi_compile __ _UVSOURCE_OBJECTSPACE
		#pragma multi_compile __ _TEXCNT_SINGLE _TEXCNT_DUO _TEXCNT_TOPBOTTOM _TEXCNT_TOP
		#pragma target 3.0 // Use shader model 3.0 target, to get nicer looking lighting

		#define DECLARE_PLANAR_TEX(name) sampler2D name; float4 name##_ST;
		#define SAMPLE_PLANAR_TEX(name,coord) tex2D(name, TRANSFORM_TEX(coord, name))

		#if _TEXCNT_SINGLE
			#define TEX_X _MainTexX
			#define TEX_Y _MainTexX
			#define TEX_Z _MainTexX
			DECLARE_PLANAR_TEX(_MainTexX)
		#elif _TEXCNT_DUO || _TEXCNT_TOP
			#define TEX_X _MainTexX
			#define TEX_Y _MainTexY
			#define TEX_Z _MainTexX
			DECLARE_PLANAR_TEX(_MainTexX)
			DECLARE_PLANAR_TEX(_MainTexY)
		#else
			#define TEX_X _MainTexX
			#define TEX_Y _MainTexY
			#define TEX_Z _MainTexZ
			DECLARE_PLANAR_TEX(_MainTexX)
			DECLARE_PLANAR_TEX(_MainTexY)
			DECLARE_PLANAR_TEX(_MainTexZ)
		#endif

		fixed4 _Color;
		half _Metallic, _Glossiness, _TexScale, _TexBlend;

		struct Input {
			float3 worldPos;
			float3 worldNormal;
		};

		void surf (Input IN, inout SurfaceOutputStandard o)
		{
			// Determine the blend weights for the 3 planar projections.
			float3 weights = IN.worldNormal;
#if _UVSOURCE_OBJECTSPACE
			weights = mul((float3x3)unity_WorldToObject, weights);
#endif
#if _TEXCNT_TOPBOTTOM || _TEXCNT_TOP
			float tb = weights.y;
#endif
			// Tighten up the blending zone:
			weights = pow(abs(weights), _TexBlend);
			// Force weights to sum to 1.0 (very important!)
			weights /= (weights.x + weights.y + weights.z).xxx;
			// Compute the UV coords for each of the 3 planar projections.
			float3 coords = IN.worldPos;
#if _UVSOURCE_OBJECTSPACE
			coords = mul(unity_WorldToObject, float4(coords, 1) ).xyz;
#endif
			coords *= _TexScale;
			// Sample color maps for each projection, at those UV coords.

			float4 colX, colY, colZ;
#if _TEXCNT_TOPBOTTOM || _TEXCNT_TOP
			colX = SAMPLE_PLANAR_TEX(TEX_X, coords.yz);
			if (tb < 0)
				colY = SAMPLE_PLANAR_TEX(TEX_Z, coords.zx);
			else
				colY = SAMPLE_PLANAR_TEX(TEX_Y, coords.zx);
			colZ = SAMPLE_PLANAR_TEX(TEX_X, coords.xy);
#else
			colX = SAMPLE_PLANAR_TEX(TEX_X, coords.yz);
			colY = SAMPLE_PLANAR_TEX(TEX_Y, coords.zx);
			colZ = SAMPLE_PLANAR_TEX(TEX_Z, coords.xy);
#endif
			fixed4 col = (colX * weights.x + colY * weights.y + colZ * weights.z) * _Color;

			/*
			float4x4 cols;
#if _TEXCNT_TOPBOTTOM || _TEXCNT_TOP
			cols[0] = SAMPLE_PLANAR_TEX(TEX_X, coords.yz);
			if (tb < 0)
				cols[1] = SAMPLE_PLANAR_TEX(TEX_Z, coords.zx);
			else
				cols[1] = SAMPLE_PLANAR_TEX(TEX_Y, coords.zx);
			cols[2] = SAMPLE_PLANAR_TEX(TEX_X, coords.xy);
#else
			cols[0] = SAMPLE_PLANAR_TEX(TEX_X, coords.yz);
			cols[1] = SAMPLE_PLANAR_TEX(TEX_Y, coords.zx);
			cols[2] = SAMPLE_PLANAR_TEX(TEX_Z, coords.xy);
#endif
			float3 cc = mul(weights, cols);
			*/

			o.Albedo = col.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = col.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
	CustomEditor "Sombra.SombraShaderGUI"
}
