#if !defined(STANDARD_DOUBLE_SIDED)
#define STANDARD_DOUBLE_SIDED

UNITY_DECLARE_TEX2D(_MainTex);

fixed4 _Color;
half _Glossiness, _Metallic;

struct Input {
	float2 uv_MainTex;
	fixed facing : VFACE;
};

UNITY_INSTANCING_BUFFER_START(Props)
UNITY_INSTANCING_BUFFER_END(Props)

void surf(Input IN, inout SurfaceOutputStandard o) {
	fixed4 c = UNITY_SAMPLE_TEX2D(_MainTex, IN.uv_MainTex) * _Color;
	o.Albedo = c.rgb;
	o.Metallic = _Metallic;
	o.Smoothness = _Glossiness;
	o.Alpha = c.a;
	// flip Z based on facing
	o.Normal.z *= IN.facing;
}

#endif // STANDARD_DOUBLE_SIDED
