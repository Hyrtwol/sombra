Shader "Cauldron/Standard/StandardCubemap"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        [NoScaleOffset] _MainTex ("Albedo (RGB CUBE)", CUBE) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        [KeywordEnum(VertexLookup, NormalLookup, UVLookup)] _LookupFunc("Lookup function", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert
        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0
        #pragma multi_compile __ _LOOKUPFUNC_NORMALLOOKUP _LOOKUPFUNC_UVLOOKUP

        #include "../CGIncludes/CauldronCubemap.cginc"

        UNITY_DECLARE_TEXCUBE(_MainTex);
        fixed4 _Color;

        half _Glossiness;
        half _Metallic;

        struct Input
        {
            LOOKUP_DECLARE;
        };

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        void vert(inout appdata_full v, out Input o)
        {
            LOOKUP_TRANSFER(o, v);
        }

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 c = LOOKUP_SAMPLE_TEXCUBE(_MainTex, IN) * _Color;
            o.Albedo = c.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
    CustomEditor "Sombra.SombraShaderGUI"
    //CustomEditor "StandardShaderGUI"
}
