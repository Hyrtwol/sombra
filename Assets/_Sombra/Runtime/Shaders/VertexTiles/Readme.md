# Vertex Tiles

    struct appdata_full
    {
        float4 vertex : POSITION;
        float4 tangent : TANGENT;
        float3 normal : NORMAL;
        float4 texcoord : TEXCOORD0;
        float4 texcoord1 : TEXCOORD1;
        float4 texcoord2 : TEXCOORD2;
        float4 texcoord3 : TEXCOORD3;
        fixed4 color : COLOR;
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };

    struct SurfaceOutputStandard
    {
        fixed3 Albedo;       // base (diffuse or specular) color
        float3 Normal;       // tangent space normal, if written
        half3  Emission;
        half   Metallic;     // 0=non-metal, 1=metal
        half   Smoothness;   // 0=rough, 1=smooth
        half   Occlusion;    // occlusion (default 1)
        fixed  Alpha;        // alpha for transparencies
    };

    UNITY_PASS_FORWARDBASE   Forward rendering base pass (main directional light, lightmaps, SH).
    UNITY_PASS_FORWARDADD    Forward rendering additive pass (one light per pass).
    UNITY_PASS_DEFERRED      Deferred shadingpass (renders g buffer).
    UNITY_PASS_SHADOWCASTER  Shadow caster and depth Texture rendering pass.
    UNITY_PASS_PREPASSBASE   Legacy deferred lighting base pass (renders normals and specular exponent).
    UNITY_PASS_PREPASSFINAL  Legacy deferred lighting final pass (applies lighting and Textures).
