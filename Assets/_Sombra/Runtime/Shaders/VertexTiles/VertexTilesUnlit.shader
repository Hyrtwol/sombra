Shader "Cauldron/VertexTiles/VertexTilesUnlit"
{
    Properties
    {
        _MainTex("Albedo (RGB 2DArray)", 2DArray) = "" {}
        _TexScale("Texture Scale", Float) = 1.0
        [PowerSlider(3.0)] _TexBlend("Blend Sharpness", Range(1,64)) = 2.0
        //[KeywordEnum(Tangent, UV0, UV2)] _RotSrc("Rotation Source", Int) = 0
    }
    CGINCLUDE
    #define V2G_LIGHTDATA
    #define V2G_LIGHTTRANSFER(o)
    #define G2F_LIGHTDATA
    #define G2F_LIGHTTRANSFER(o)
    ENDCG
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM

            #pragma require geometry
            #pragma require 2darray
            //#pragma target 4.0
            #pragma vertex vert
            #pragma geometry geom_surf_trio
            #pragma fragment frag

#ifdef CAULDRON_FOG
            #pragma multi_compile_fog
#endif
            #define TRIO_PACKUV

            #include "UnityCG.cginc"
            #include "VertexTilesUtils.cginc"

            UNITY_DECLARE_TEX2DARRAY(_MainTex);
        
            struct appdata
            {
                float4 vertex     : POSITION;
                float3 normal     : NORMAL;
                float4 texcoord   : TEXCOORD0; // x = tex-index, y = tex-stretch, zw = tex-offset
                float4 texcoord3  : TEXCOORD3; // xyz = texture-tangent, w = ?
              //float4 texcoord4  : TEXCOORD4; // x = slope, y = curvature, z = altitude (elevation), w = temperature
              //float4 texcoord5  : TEXCOORD5; // x = fertility, y = humidity
            };

            // vertex-to-geometry interpolation data
            struct v2g_surf_trio
            {
                UNITY_POSITION(pos);
                VT_V2G_TDATA(0, 1, 2, 3)
                V2G_LIGHTDATA
            };

            // geometry-to-fragment interpolation data
            struct g2f_surf_trio
            {
                UNITY_POSITION(pos);
            #ifdef TRIO_PACKUV
                VT_G2F_TDATA_PACKED(0, 1, 2)
            #else
                VT_G2F_TDATA(0, 1, 2, 3)
            #endif
                G2F_LIGHTDATA
            };

            // vertex shader
            v2g_surf_trio vert(appdata v)
            {
                v2g_surf_trio o;
                o.pos = UnityObjectToClipPos(v.vertex);
                //o.worldNormal = UnityObjectToWorldNormal(v.normal);
                VT_TRANSFER_APPDATA(v, o)
                return o;
            }

            // geometry shader
            #include "VertexTilesGeometry.cginc"

            // fragment shader
            fixed4 frag(g2f_surf_trio i) : SV_Target
            {
                float3 blendWeights = CALC_BLEND_WEIGHTS(i);
                fixed4 color = TRIO_SAMPLE_TEX2DARRAY_WEIGHTED(_MainTex, i, blendWeights);
#ifdef CAULDRON_FOG
                UNITY_APPLY_FOG(i.fogCoord, color); // apply fog
#endif
                return color;
            }

            ENDCG
        }
    }
    //FallBack "Diffuse"
}
