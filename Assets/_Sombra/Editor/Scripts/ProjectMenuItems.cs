using System.Text;
using UnityEditor;
using UnityEngine;

namespace Sombra.Editor
{
    [InitializeOnLoad]
    public static class ProjectMenuItems
    {
        public const string ItemNamePrefix = "Sombra"; //ArtifactInfo.Name;
        public const string WindowNamePrefix = "Window/" + ItemNamePrefix;

        static ProjectMenuItems() { }

        [MenuItem(ItemNamePrefix + "/Git repo")] public static void GitRepo() => Application.OpenURL("https://bitbucket.org/Hyrtwol/sombra/");
        [MenuItem(ItemNamePrefix + "/Project home page")] public static void ProjectPage() => Application.OpenURL("http://hyrtwol.dk/");

        [MenuItem(ItemNamePrefix + "/" + nameof(About))]
        //[MenuItem(WindowNamePrefix + "/" + nameof(About))]
        public static void About()
        {
            var info = new StringBuilder();
            info.AppendLine("Sombra a la Cour");
            info.AppendLine("http://hyrtwol.dk");
            Debug.Log(info);
        }

        //[MenuItem(ItemNamePrefix + "/" + nameof(FixIt))]
        //public static void FixIt()
        //{
        //    var mesh = AssetDatabase.LoadAssetAtPath<Mesh>("Assets/Prefabs/VertexTilesSphere.asset");
        //    Debug.Log(mesh);
        //    mesh.tangents = null;
        //}
    }
}
