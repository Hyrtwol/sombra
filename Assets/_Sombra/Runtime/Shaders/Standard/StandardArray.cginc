#if !defined(STANDARD_ARRAY)
#define STANDARD_ARRAY

#include "../CGIncludes/PackedProperties.cginc"

int _TexIdx;

UNITY_DECLARE_TEX2DARRAY(_MainTex);
#ifdef PACKED_TEX
UNITY_DECLARE_TEX2DARRAY(_PackedTex);
#endif
#ifdef _NORMALMAP
UNITY_DECLARE_TEX2DARRAY(_BumpMap);
#endif

struct Input {
	float2 uv_MainTex;
};

UNITY_INSTANCING_BUFFER_START(Props)
UNITY_INSTANCING_BUFFER_END(Props)

void surf(Input IN, inout SurfaceOutputStandard o)
{
    float3 uvw = float3(IN.uv_MainTex, _TexIdx);
    fixed4 color = UNITY_SAMPLE_TEX2DARRAY(_MainTex, uvw);
#if defined(PACKED_TEX)
    fixed4 packed = UNITY_SAMPLE_TEX2DARRAY(_PackedTex, uvw);
#endif
    TRANSFER_PACKED(o)

#ifdef _NORMALMAP
    half4 packednormal = UNITY_SAMPLE_TEX2DARRAY(_BumpMap, uvw);
  #ifdef _NORMALMAP_SCALE
    o.Normal = UnpackScaleNormal(packednormal, _BumpScale);
  #else
    o.Normal = UnpackNormal(packednormal);
  #endif
#endif
}

#endif // STANDARD_ARRAY
