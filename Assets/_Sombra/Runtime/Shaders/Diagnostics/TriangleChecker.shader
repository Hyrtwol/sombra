Shader "Cauldron/Diagnostics/TriangleChecker"
{
    Properties
    {
        [KeywordEnum(RGB, Outline)] _Mode("Draw Mode", Int) = 0
        [PowerSlider(3.0)] _BlendExponent("Blend Sharpness", Range(1,64)) = 1.0
        _BlendOffset("Blend Offset", Range(-2, 2)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM

            #pragma vertex vert
            #pragma geometry geom
            #pragma fragment frag
            #pragma multi_compile _ _MODE_OUTLINE

            #include "UnityCG.cginc"

            half _BlendExponent;
            #if _MODE_OUTLINE
                half _BlendOffset;
            #endif

            struct v2g
            {
                float4 vertex : SV_POSITION;
            };

            struct g2f
            {
                float4 vertex : SV_POSITION;
                float3 color: COLOR;
            };

            v2g vert(float4 vertex : POSITION)
            {
                v2g o;
                o.vertex = UnityObjectToClipPos(vertex);
                return o;
            }

            #define ADD_VTX(i,r,g,b) o.vertex = input[i].vertex; o.color = float3(r,g,b); triStream.Append(o);

            [maxvertexcount(3)]
            void geom(triangle v2g input[3], inout TriangleStream<g2f> triStream)
            {
                g2f o;
                #if _MODE_OUTLINE
                    ADD_VTX(0, 0, 1, 1)
                    ADD_VTX(1, 1, 0, 1)
                    ADD_VTX(2, 1, 1, 0)
                #else // RGB
                    ADD_VTX(0, 1, 0, 0)
                    ADD_VTX(1, 0, 1, 0)
                    ADD_VTX(2, 0, 0, 1)
                #endif
            }

            fixed4 frag(g2f i) : SV_Target
            {
                float3 weights = i.color;
                #if _MODE_OUTLINE
                    weights = pow(weights + _BlendOffset, _BlendExponent);
                    float m = max(max(weights.x, weights.y), weights.z);
                    //m = saturate(m);
                    return m.xxxx;
                #else // RGB
                    weights = pow(weights, _BlendExponent);
                    weights /= (weights.x + weights.y + weights.z).xxx;
                    return fixed4(weights, 1);
                #endif
            }

            ENDCG
        }
    }
}
